#!/bin/bash

version=1.2
array=("4k" "64k" "128k" "1m")
echo -en "Welcome to FIO service monitor initializer ver $version\n"
echo -en "Uninstalling existing fio-exporter\n"

if ! command -v helm &> /dev/null
then
    echo "helm command could not be found!"
    exit
fi

sleep 1


helm list -n monitoring  | grep "chart" | while read -r release namespace
do
    echo "Uninstalling Helm release: $release in namespace $namespace"
    helm uninstall "$release" -n "$namespace"
    helm uninstall "$release" -n monitoring
    helm uninstall "$release" -n default
done

sleep 10

cd /etc/wrapper-empty/

for str in "${array[@]}"; do
    echo -en "\nInstalling fio-exporter based on blocksize: $str\n"
    helm install . --values values.yaml --set latency_args.bs=$str --generate-name
    sleep 1
done

echo $?
