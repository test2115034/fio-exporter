package main

import (
	"bufio"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

var fioBinPath = "/root/handler/fio_bench_build/main"
var action = ""

func main() {
	timestamp := time.Now().Format("02/01/2006 15:04:05")
	log.Println("Application is running -", timestamp)
	http.HandleFunc("/runnow", handleRequest)
	log.Fatal(http.ListenAndServe(":9080", nil))
}

type Request struct {
	AdditionalFlags string `json:"additionalFlags"`
}

func isFioRunning() bool {
	cmd := exec.Command("pgrep", "fio")
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Printf("Error checking if fio is running: %v", err)
		return false
	}
	return len(strings.TrimSpace(string(output))) > 0
}

func nodeRegistration(action string) {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalf("Failed to get hostname: %v", err)
	}
	url := "http://merge-exporter.rafrefet.local/" + action + "?name=" + hostname + "&address=10.100.102.235:9996/metrics"
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("Failed to send unregister request: %v", err)
	}
	defer resp.Body.Close()
	log.Printf("Node %s unregistered with response status: %s", hostname, resp.Status)
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	timestamp := time.Now().Format("02/01/2006 15:04:05")
	log.Printf("Received request at %s:\nMethod: %s\nURL: %s\nHeaders: %v\n", timestamp, r.Method, r.URL.Path, r.Header)
	nodeRegistration("register")

	var req Request
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Printf("Error decoding JSON request: %v", err)
		return
	}

	log.Printf("Received JSON:\n%s\n", req.AdditionalFlags)

	concatenatedFlags := "-benchmark=custom " + req.AdditionalFlags
	fullCommand := fioBinPath + " " + concatenatedFlags

	go func() {
		log.Println("Handler is running -", timestamp)
		cmd := exec.Command("sh", "-c", fullCommand)
		log.Printf("Running command: %s", fullCommand)

		cmdReader, err := cmd.StdoutPipe()
		if err != nil {
			log.Fatalf("Error creating StdoutPipe for Cmd: %v", err)
		}

		scanner := bufio.NewScanner(cmdReader)
		go func() {
			for scanner.Scan() {
				log.Printf("fio: %s", scanner.Text())
			}
		}()

		if err := cmd.Start(); err != nil {
			log.Fatalf("Error starting Cmd: %v", err)
		}

		// Create a timer to call unregisterNode after 2 minutes
		timer := time.NewTimer(2 * time.Minute)
		select {
		case <-timer.C:
			nodeRegistration("unregister")
		}

		if err := cmd.Wait(); err != nil {
			log.Printf("fio finished with error: %v", err)
		}
	}()

	log.Println("Handling request -", timestamp)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Received request and started processing"))
}
